/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
var MyToastAndroid = require('MyToastAndroid');
var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} = React;

var AwesomeProject = React.createClass({
  _onPress: function() {
    MyToastAndroid.show('Awesome', MyToastAndroid.SHORT);
  },
  render: function() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit index.android.js
        </Text>
        <Text style={styles.instructions}>
          Shake or press menu button for dev menu
        </Text>
        <TouchableHighlight style={styles.button} onPress={this._onPress.bind(this)}>
            <Text style={styles.buttonText}>Press me for short toast.</Text>
        </TouchableHighlight> 
      </View>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);
